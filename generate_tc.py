from os import write
import string    
import random

def get_Q1(text):
    return int(len(text) * 0.25)

def get_Q2(text):
    return int(len(text) * 0.5)

def get_pattern_len_short(text):
    patternLen = random.randint(0, get_Q1(text))
    if patternLen == 0:
        patternLen = 1
    return patternLen

def get_pattern_len_long(text):
    patternLen = random.randint(get_Q1(text), get_Q2(text))
    return patternLen


def generate_short_text_short_pattern():
    file = open('backnav-boyermoore/testcase_SS.txt', 'w')
    for i in range(1000):
        S = random.randint(1, 1000)
        text = ''.join(random.choices(string.ascii_uppercase + " ", k = S))
        patternLen = get_pattern_len_short(text)
        firstIndexPatt = random.randint(0, len(text)-1)
        lastIndexPatt = firstIndexPatt + patternLen
        if lastIndexPatt > len(text):
            lastIndexPatt = len(text)
            firstIndexPatt = lastIndexPatt - patternLen
        
        pattern = text[firstIndexPatt:lastIndexPatt]
        file.write(text + "\n")
        file.write(pattern + "\n")
        file.write(str(len(text)) + " " + str(len(pattern)) + " " + str(firstIndexPatt) + "\n")
        file.write("\n")
    file.write("eof")
    print("Short text, short pattern DONE!\n")

def generate_short_text_long_pattern():
    file = open('backnav-boyermoore/testcase_SL.txt', 'w')
    for i in range(1000):
        S = random.randint(1, 1000)
        text = ''.join(random.choices(string.ascii_uppercase + " ", k = S))
        patternLen = get_pattern_len_long(text)
        firstIndexPatt = random.randint(0, len(text)-1)
        lastIndexPatt = firstIndexPatt + patternLen
        if lastIndexPatt > len(text):
            lastIndexPatt = len(text)
            firstIndexPatt = lastIndexPatt - patternLen
         
        pattern = text[firstIndexPatt:lastIndexPatt]
        file.write(text + "\n")
        file.write(pattern + "\n")
        file.write(str(len(text)) + " " + str(len(pattern)) + " " + str(firstIndexPatt) + "\n")
        file.write("\n")
    file.write("eof")
    print("Short text, long pattern DONE!\n")

def generate_long_text_short_pattern():
    file = open('backnav-boyermoore/testcase_LS.txt', 'w')
    for i in range(1000):
        S = random.randint(1001, 5000)
        text = ''.join(random.choices(string.ascii_uppercase + " ", k = S))
        patternLen = get_pattern_len_short(text)
        firstIndexPatt = random.randint(0, len(text)-1)
        lastIndexPatt = firstIndexPatt + patternLen
        if lastIndexPatt > len(text):
            lastIndexPatt = len(text)
            firstIndexPatt = lastIndexPatt - patternLen
         
        pattern = text[firstIndexPatt:lastIndexPatt]
        file.write(text + "\n")
        file.write(pattern + "\n")
        file.write(str(len(text)) + " " + str(len(pattern)) + " " + str(firstIndexPatt) + "\n")
        file.write("\n")
    file.write("eof")
    print("Long text, short pattern DONE!\n")

def generate_long_text_long_pattern():
    file = open('backnav-boyermoore/testcase_LL.txt', 'w')
    for i in range(1000):
        S = random.randint(1001, 5000)
        text = ''.join(random.choices(string.ascii_uppercase + " ", k = S))
        patternLen = get_pattern_len_long(text)
        firstIndexPatt = random.randint(0, len(text)-1)
        lastIndexPatt = firstIndexPatt + patternLen
        if lastIndexPatt > len(text):
            lastIndexPatt = len(text)
            firstIndexPatt = lastIndexPatt - patternLen
        
        pattern = text[firstIndexPatt:lastIndexPatt]
        file.write(text + "\n")
        file.write(pattern + "\n")
        file.write(str(len(text)) + " " + str(len(pattern)) + " " + str(firstIndexPatt) + "\n")
        file.write("\n")
    file.write("eof")
    print("Long text, long pattern DONE!\n")

generate_short_text_short_pattern()
generate_short_text_long_pattern()
generate_long_text_short_pattern()
generate_long_text_long_pattern()
