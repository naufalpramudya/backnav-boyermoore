import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Simulator {
    public static void main(String[] args) {
        // comment/uncomment bagian dari "ini buat 'nama algoritma'" hingga
        // mencetak kecepatan rata-rata untuk melihat kerja algoritma yang diinginkan

        String text, pattern, len, end;
        try (BufferedReader fr = new BufferedReader(new FileReader("backnav-boyermoore/testcase_LL.txt"))) {
            text = "";
            double averageTime = 0;
//            System.out.println("ini buat boyer moore");
            for (int i = 1; i < 1001; i++){
                if (text.equals("eof")) break;
                System.out.println(String.format("READ FROM testcase-%d", i));
                text = fr.readLine();
                pattern = fr.readLine();
                len = fr.readLine();
                end = fr.readLine();
                String[] lenSplit = len.split(" ");
                System.out.println(String.format("Text length: %s \n Pattern length: %s",lenSplit[0],lenSplit[1]));
                System.out.println(String.format("Pattern found at index: %d", BoyerMoore.strStr(text, pattern)));
                long startTime = System.nanoTime();
                BoyerMoore.strStr(text, pattern);
                long endTime   = System.nanoTime();
                long totalTime = endTime - startTime;
                double seconds = (double)totalTime / 1_000_000_000.0;
                averageTime += seconds;
                System.out.println(String.format("total time: %f second", seconds));
                System.out.println("==================================================");
            }
            System.out.println(String.format("Average time taken by boyer-moore: %f",averageTime/1000));
//=========================================================================================================
//            System.out.println("ini buat Back-Nav");
//            for (int j =1; j<1001;j++) {
//                if (text.equals("eof")) {
//                    break;
//                }
//                System.out.println(String.format("READ FROM testcase %d",j));
//                text = fr.readLine();
//                pattern = fr.readLine();
//                len = fr.readLine();
//                end = fr.readLine();
//                String[] lenSplit = len.split(" ");
//                System.out.println(String.format("Text length: %s \n Pattern length: %s",lenSplit[0],lenSplit[1]));
//                System.out.println(String.format("Pattern found at index: %d", BSMA.backnavigation(text, pattern)));
//                long startTime = System.nanoTime();
//                BSMA.backnavigation(pattern, text);
//                long endTime = System.nanoTime();
//                long totalTime = endTime - startTime;
//                double seconds = (double) totalTime / 1_000_000_000.0;
//                averageTime += seconds;
//                System.out.println(String.format("total time: %f second", seconds));
//                System.out.println("==================================================");
//            }
//            System.out.println(String.format("Average time taken by back-nav: %f",averageTime/1000));
        } catch (IOException ex) {
            System.out.println("File not Found!");
        }    
    }
}
