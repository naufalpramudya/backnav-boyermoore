# Runtime Analysis of Back Navigation and Boyer-Moore Algorithm

This repository for DAA Paper is created by:
1. **Muhammad Fahreza Pratama Wijayanto** - 1906351051
2. **Naufal Pramudya Yusuf** - 1906293266

## Files

1. __*BSMA.java*__ adalah file java untuk algoritma Back Navigation
2. __*BoyerMoore.java*__ adalah file java untuk algoritma Boyer-Moore
3. __*Simulator.java*__ adalah file java untuk menjalankan kedua algoritma di atas
4. __*generate_tc.py*__ adalah file python yang digunakan untuk membuat testcase
5. Dokumen-dokumen test yang masing-masing berisi 1000 testcase dengan keterangan sebagai berikut:

Tiap testcase terdiri dari 3 line secara berurutan, yaitu teks, pattern, dan detail ukuran (panjang teks, panjang pattern, index pattern pada teks). Masing-masing testcase dipisah dengan whitespace dan akhir dari dokumen ditandai dengan "eof".
- __*testcase_SS.txt*__ adalah dokumen yang terdiri dari testcase dengan ukuran **teks pendek** dan **pattern pendek** (relatif terhadap ukuran teks)
- __*testcase_SL.txt*__ adalah dokumen yang terdiri dari testcase dengan ukuran **teks pendek** dan **pattern panjang** (relatif terhadap ukuran teks)
- __*testcase_LS.txt*__ adalah dokumen yang terdiri dari testcase dengan ukuran **teks panjang** dan **pattern pendek** (relatif terhadap ukuran teks)
- __*testcase_LL.txt*__ adalah dokumen yang terdiri dari testcase dengan ukuran **teks panjang** dan **pattern panjang** (relatif terhadap ukuran teks)

6. __*testcaseSample.txt*__ adalah dokumen testcase sampel yang berisi 10 testcase


