public class BSMA{
    public static int backnavigation(String text, String pattern) {
        int n = pattern.length();
        int i = text.length() - 1;

        char[] t = text.toCharArray();
        char[] p = pattern.toCharArray();
        while (i >= 0) {
            if (t[i] == p[n - 1] & i - (n - 1) >= 0) {
                int m = 1;
                while (m <= (n)) {
                    if (t[i - (n - m)] != p[m - 1]) {
                        break;
                    }
                    m++;
                    if (m == n) {
                        return i - (n - 1);
                    }
                }
            }
            i--;
        }
        return -1;
    }
}

